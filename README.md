# Galaxian
Galaxian is a Web Project Manager. It's goal is to ease project creation, setup and development.
***
### Goals
- Ability to create a project and manage it's different properties;
- Ability to view a list of projects and easily access/delete them;
- Ability to start/stop different service for each project ( possibly in it's own shell );
- Ability to add custom 'project types' with their own properties and services;

## Definitions
- **Project** - A project is a folder which will serve as the root for all watch/run/build commands.
- **Property** -  A property is anything related and/or that defines a project. For example, the framework, the css pre-processor, the javascript compiler, etc.
- **Service** - A service is anything that runs automatically during development and/or to build/compile a project. For example, file watchers, browser hot-reloading, etc.

## Todos
- [ ] Create the Basic App Layout
- [ ] Allow creating projects
- [ ] Allow modifying/deleting projects
- [ ] Allow viewing a list of projects
